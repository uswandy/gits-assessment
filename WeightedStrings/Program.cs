﻿using System;
using System.Collections.Generic;

public class WeightedStrings
{
    public static List<string> CheckQueries(string values, List<int> queries)
    {
        List<string> results = new List<string>();
        Dictionary<string, int> weights = CalculateWeights(values);
        foreach (var query in queries)
        {
            Console.WriteLine("query {0}", query);
            if (weights.ContainsValue(query))
                results.Add("Yes");
            else
                results.Add("No");
        }
        return results;
    }

    private static Dictionary<string, int> CalculateWeights(string values)
    {
        Dictionary<string, int> weights = new Dictionary<string, int>();  
        for (int i = 0; i < values.Length; i++)
        {
            int sum = values[i] - 'a' + 1;
            weights[values[i].ToString()] = sum;

            int j = i + 1;
            while (j < values.Length && values[j] == values[i])
            {
                sum += values[j] - 'a' + 1;
                string substring = values.Substring(i, j - i + 1);
                weights[substring] = sum;
                j++;
            }
        }
        return weights;
    }

    public static void Main()
    {
        string input = "abbcccd";
        List<int> queries = new List<int> { 1, 3, 9, 8 };
        List<string> results = CheckQueries(input, queries);

        Console.WriteLine(string.Join(", ", results));
        
        string input2 = "gits";
        List<int> queries2 = new List<int> { 1, 3, 9, 8, 20 };
        List<string> results2 = CheckQueries(input2, queries2);

        Console.WriteLine(string.Join(", ", results2));
    }
}
