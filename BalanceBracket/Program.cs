﻿using System;
using System.Collections;
using System.Collections.Generic;

public class BalancedBrackets
{
    public static bool IsBracketBalacned(string values)
    {
        // string[] values = { "{[()]}", "{[(])}", "{(([])[])[]}", "{ [ ( * ) ] }", "(ABCDEF}" };
        Stack<char> bracketStack = new Stack<char>();
        foreach (char item in values)
        {
            if (item == '(' || item == '[' || item == '{')
            {
                bracketStack.Push(item);
            }
            else if (item == ')' && (bracketStack.Count == 0 || bracketStack.Pop() != '('))
            {
                return false;
            }
            else if (item == ']' && (bracketStack.Count == 0 || bracketStack.Pop() != '['))
            {
                return false;
            }
            else if (item == '}' && (bracketStack.Count == 0 || bracketStack.Pop() != '{'))
            {
                return false;
            }
        }

        return bracketStack.Count == 0;
    }

    public static void Main()
    {
        string[] testInputs = { "{[()]}", "{[(])}", "{(([])[])[]}", "{ [ ( * ) ] }", "(ABCDEF}" };

        foreach (string testInput in testInputs)
        {
            Console.WriteLine(IsBracketBalacned(testInput) ? "YES" : "NO");
        }
    }
}
