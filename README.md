
# GITS Assessment - Hendra

C# assessment


## Test 1
## Test 2
## Balanced Brackets
Ukuran kompleksitas dari jawaban test "Balanced Brackets" adalah O(n). disebut dengan Notasi O Besar atau Big-O Notation.

n adalah panjang string input.


O(1) berarti kinerja algoritma konstan, artinya waktu atau ruang yang dibutuhkan tidak berubah seiring dengan pertambahan ukuran input.

O(n) berarti kinerja algoritma linier, artinya waktu atau ruang yang dibutuhkan tumbuh sebanding dengan ukuran input n.

https://medium.com/bee-solution-partners/penjelasan-sederhana-tentang-time-complexity-dan-big-o-notation-4337ba275cfe